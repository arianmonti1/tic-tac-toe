# Tic-Tac-Toe
## Play Tic Tac Toe on your teminal🎲😍
![Image of Yaktocat](https://github.com/alirezainjast/tic-tac-toe/blob/master/screenShot.png)
### How to run
1. install `Python`.
1. Run these commands:
```
git clone https://github.com/arianmonti/tic-tac-toe.git
cd tic-tac-toe
pip install -r requirements.txt
python two_player.py
```
> if you are using `MICROSOFT WINDOWS` and you want to play:
* replace `os.system('clear')` with `os.system('cls')` in `two-player.py`
* use `POWERSHELL`
> OS Support: Linux, Mac OS
